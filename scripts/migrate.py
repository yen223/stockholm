#!/usr/bin/env python
from __future__ import absolute_import
# from stockholm import db
from sqlalchemy import MetaData, Table, Column, Integer, update, insert, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa

'''
This script is to migrate from float-based prices
to the new integer-based prices.
'''
meta = MetaData()
new_stockquote = Table(
    'stock_quotes',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('stock_id', sa.Integer, sa.ForeignKey('stock.id')),
    sa.Column('open_price', sa.Integer),
    sa.Column('close_price', sa.Integer),
    sa.Column('high', sa.Integer),
    sa.Column('low', sa.Integer),
    sa.Column('volume', sa.Integer),
    sa.Column('buy', sa.Integer),
    sa.Column('sell', sa.Integer),
    sa.Column('buy_volume', sa.Integer),
    sa.Column('sell_volume', sa.Integer),
    sa.Column('transaction_time', sa.DateTime)
)

stock = Table(
    'stock',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.String(255)),
    sa.Column('stock_code', sa.String(6)),
    )
from sqlalchemy import and_, func

def column_windows(session, column, windowsize):
    """Return a series of WHERE clauses against 
    a given column that break it into windows.

    Result is an iterable of tuples, consisting of
    ((start, end), whereclause), where (start, end) are the ids.

    Requires a database that supports window functions, 
    i.e. Postgresql, SQL Server, Oracle.

    Enhance this yourself !  Add a "where" argument
    so that windows of just a subset of rows can
    be computed.

    """
    def int_for_range(start_id, end_id):
        if end_id:
            return and_(
                column>=start_id,
                column<end_id
            )
        else:
            return column>=start_id

    q = session.query(
                column, 
                func.row_number().\
                        over(order_by=column).\
                        label('rownum')
                ).\
                from_self(column)
    if windowsize > 1:
        q = q.filter("rownum %% %d=1" % windowsize)

    intervals = [id for id, in q]

    while intervals:
        start = intervals.pop(0)
        if intervals:
            end = intervals[0]
        else:
            end = None
        yield int_for_range(start, end)

def windowed_query(q, column, windowsize):
    """"Break a Query into windows on a given column."""

    for whereclause in column_windows(
                                        q.session, 
                                        column, windowsize):
        for row in q.filter(whereclause).order_by(column):
            yield row

def quick_mapper(table):
    Base = declarative_base()
    class GenericMapper(Base):
        __table__ = table
    return GenericMapper

def make_session(connection_string):
    engine = create_engine(connection_string, echo=False, convert_unicode=True)
    Session = sessionmaker(bind=engine)
    return Session(), engine

def convert(float_val):
    if float_val is not None:
        return int(round(float_val*1000)) #Tick size = 0.5 sen
    else:
        return None

def pull_data(from_db, to_db, tables):
    source, sengine = make_session(from_db)
    smeta = MetaData(bind=sengine)
    destination, dengine = make_session(to_db)

    for table_name in tables:
        print 'Processing', table_name
        print 'Pulling schema from source server'
        table = Table(table_name, smeta, autoload=True)
        print 'Creating table on destination server'
        # table.metadata.create_all(dengine)
        NewRecord = quick_mapper(table)
        columns = table.columns.keys()
        print 'Transferring records'
        for record in source.query(table).yield_per(100):
            data = dict(
                [(str(column), getattr(record, column)) for column in columns]
            )
            destination.merge(NewRecord(**data))
        destination.commit()

    print "Migrating stock_quotes"
    # Migrate stock_quote
    stock_quotes = Table('stock_quotes', smeta, autoload=True)
    # meta.create_all(dengine)
    NewRecord = quick_mapper(new_stockquote)
    q = source.query(stock_quotes)
    data_list = []
    lconvert = convert
    for idx, record in enumerate(q.yield_per(1000)):
        # print "Copying record #{0}".format(idx)
        data = {
            'id': record.id,
            'stock_id': record.stock_id,
            'open_price': lconvert(record.open_price),
            'close_price': lconvert(record.close_price),
            'high': lconvert(record.high),
            'low': lconvert(record.low),
            'buy': lconvert(record.buy),
            'sell': lconvert(record.sell),
            'volume': record.volume,
            'buy_volume': record.buy_volume,
            'sell_volume': record.sell_volume,
            'transaction_time': record.transaction_time,
        }
        data_list.append(data)
        # destination.merge(NewRecord(**data))
        if idx%1000 == 0:
            print 'Committing changes: {}'.format(idx)
            dengine.execute(
                new_stockquote.insert(),
                data_list
                )
            data_list = []
            # destination.commit()
    else:
        dengine.execute(
                new_stockquote.insert(),
                data_list
                )
        print 'Committing last changes'
        # destination.commit()
    

if __name__ == '__main__':
    old_string = "postgresql://stockholm@localhost:5432/bursadb"
    new_string = "postgresql://stockholm@localhost:5432/stockholm"
    pull_data(old_string, new_string, ['stock'])
