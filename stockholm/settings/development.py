from .common import *

BROKER_URL = 'amqp://weiyenlee:123456@127.0.0.1:5672/stockholm'
SQLALCHEMY_DATABASE_URI = "postgresql://weiyenlee@localhost:5432/stockholm"
SQLALCHEMY_ECHO = True
CELERYBEAT_SCHEDULE = {
    # Executes every Monday morning at 7:30 A.M
    'scrape-data': {
        'task': 'stockholm.livefeed.tasks.scrape',
        'schedule': crontab(minute='*'),
        'kwargs':{
            "url": "http://www.bursastation.com/prices.pl?action=prices&type=15&alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        },
    },
    'test-post-please-ignore': {
        'task': 'stockholm.livefeed.tasks.test',
        'schedule': crontab(minute='*/5'),
    },
}