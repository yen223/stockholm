from __future__ import absolute_import
from celery.schedules import crontab
import os
from .log_settings import *

# Celery Settings
CELERY_TIMEZONE = "Asia/Kuala_Lumpur"
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
CELERY_STORE_ERRORS_EVEN_IF_IGNORED = False
CELERYD_HIJACK_ROOT_LOGGER = False
CELERY_IMPORTS = ["stockholm.livefeed.tasks",]
CELERY_TASK_RESULT_EXPIRES = 300 
CELERY_ROUTES = {
    'stockholm.livefeed.tasks.write_to_database': {'queue':'db_writers'}
}
# CELERY_TASK_SERIALIZER = 'yaml'
# CELERY_RESULT_SERIALIZER = 'yaml'
# CELERY_ACCEPT_CONTENT = ['json', 'yaml']
CELERYBEAT_SCHEDULE = {
    # Executes every Monday morning at 7:30 A.M
    'scrape-data': {
        'task': 'stockholm.livefeed.tasks.scrape',
        'schedule': crontab(minute='*', hour='7-18', day_of_week='mon-fri'),
        'kwargs':{
            "url": "http://www.bursastation.com/prices.pl?action=prices&type=15&alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        },
    },
    'test-post-please-ignore': {
        'task': 'stockholm.livefeed.tasks.test',
        'schedule': crontab(minute='*/5'),
    },
}

