import logging
import logging.handlers


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    # 'filters': {
        # 'special': {
        #     '()': 'project.logging.SpecialFilter',
        #     'foo': 'bar',
        # }
    # },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'logging.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
        },
        'error_log':{
            'level':'WARNING',
            'class':'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'formatter': 'verbose',
            'filename': 'error.log'
        },
        'info_log':{
            'level':'INFO',
            'class':'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'formatter': 'verbose',
            'filename': 'info.log'
        },
        'gmail_log':{
            'level': 'ERROR',
            'class': 'logging.handlers.SMTPHandler',
            'mailhost': ("smtp.gmail.com", 587),
            'fromaddr': 'from@example.com',
            'toaddrs': ['to_1@example.com', 'to_2@example.com'],
            'subject': "Error from Stockholm",
            'credentials': ('username', 'password') #Override this.
        }
    },
    'loggers': {
        'celery.task': {                  
            'handlers': ['gmail_log', 'info_log', 'error_log'],        
            'level': 'INFO',  
            'propagate': True  
        },
        '': {                  
            'handlers': ['gmail_log', 'info_log', 'error_log'],        
            'level': 'INFO',  
            'propagate': True  
        }
    }
}

