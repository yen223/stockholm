$(function(){
    var updateInterval = 30000; //Fetch data ever x milliseconds
    var realtime = "on"; //If == to on then fetch data every x seconds. else stop fetching

    function getDecimalPrice(value) {
        return value/1000;
    }

    var interactive_plot;
    function update(stockCodes, initialize) {
        var datasets = [];
        var requests = [];
        var startDate = moment().format('YYYYMMDD[T]HHmmss');
        var endDate = moment().add('days', 1).format('YYYYMMDD[T000000]');

        $.each(stockCodes, function(index, value){
            var url = STOCK_QUOTE_URL + "?code=" + value + "&start=" + startDate + "&end=" + endDate;
            // Generate an ajax call for each tracked stock
            requests.push($.ajax(url));
        });
        
        // Calls all the ajax calls.
        var defer = $.when.apply($, requests);
        defer.done(function(){
            var dataset = [];
            if (requests.length == 1){
                console.log(arguments);
            } else {
                $.each(arguments, function(index, responseData){
                    // console.log(responseData);
                    response = responseData[0]
                    data = $.map(response['quotes'], function(elem){
                        return [[elem.transaction_timestamp, getDecimalPrice(elem.buy)]];
                    });
                    label = response['stock_name'];
                    // console.log(label);
                    dataset.push({
                        'label': label,
                        'data': data,
                    });
                })
            }
            
            if (initialize){
                interactive_plot = $.plot("#interactive",dataset, {
                    grid: {
                        borderColor: "#f3f3f3",
                        borderWidth: 1,
                        tickColor: "#f3f3f3"
                    },
                    series: {
                        shadowSize: 0, // Drawing is faster without shadows
                        // color: "#3c8dbc"
                    },
                    lines: {
                        //fill: true, //Converts the line chart to area chart
                        color: "#3c8dbc"
                    },
                    yaxis: {
                        // min: 2.3,
                        // max: 2.7,
                        show: true
                    },
                    xaxis: {
                        mode: "time",
                        show: true,
                        tickSize: [30, "minute"],
                        timezone: "browser",
                    },
                    legend: { 
                        show: true, 
                        container: null,
                        position: 'ne',
                    },

                    colors: ['#FF0000', '#FF7400', '#009999', '#00CC00'],
                });     
            } else {
                interactive_plot.setData(dataset);
                interactive_plot.draw();
            } 

            if (realtime === "on")
                setTimeout(update, updateInterval, stockCodes, false);
        })
    }
   
    update(["5099", "2259", "1818"], true);
});
