from bs4 import BeautifulSoup
from stockholm.celery_app import app
from celery import task, subtask, group, shared_task
from celery.utils.log import get_task_logger
# from stockholm.database import get_session_scope, get_or_create
from stockholm import db
from stockholm.core.database import get_or_create, get_session_scope
from stockholm.quotes.models import StockQuote, Stock
from datetime import datetime
import locale
import logging
import re
import requests


locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )
logger = get_task_logger(__name__)

DEFAULT_URL = "http://www.bursastation.com/prices.pl?action=prices&type=15&alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def _process_string(text):
    if text == "-":
        return "0"
    else:
        return text


@app.task(ignore_result=True)
def write_to_database(obj):
    logger.info("Writing to database...")
    # logger.info("Writing {0} objects...".format(len(objects)))
    # for obj in objects:
    if not obj: 
        logger.warning("write_to_database: Received None obj.")
        return

    with get_session_scope() as session:
        try:
            logger.info("Writing: {0}".format(obj['name']))
            logger.debug("Writing: {0}".format(obj))
            # with get_session_scope() as session:
            stock, _ = get_or_create(session, Stock, name=obj['name'])
            stock.stock_code = obj['stock_code']

            stockquote, created = get_or_create(session, StockQuote, stock=stock, 
                transaction_time=obj['transaction_time'])
            stockquote.volume = obj['volume']
            stockquote.buy_volume = obj['buy_volume']
            stockquote.buy = obj['buy']
            stockquote.sell = obj['sell']
            stockquote.sell_volume = obj['sell_volume']
            stockquote.high = obj['high']
            stockquote.low  = obj['low']
            # db.session.commit()
            if created:
                logger.info("{0}: Quote added.".format(stock))
            else:
                logger.info("{0}: Quote updated.".format(stock))
        except Exception as e:
            logger.exception("Exception encountered while processing {0}: {1}".format(obj, e))
        


@app.task(bind=True)
def fetch_site(self, url):
    try:
        logger.info("Fetching site from {}".format(url))
        # url = "http://www.bursastation.com/prices.pl?action=prices&type=15&alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        req = requests.get(url)
        html = req.text
        return html
    except requests.exceptions.RequestException as exc:
        logger.warning("Exception caught while retrieving site: {}".format(exc))
        raise self.retry(exc=exc, countdown=30)


@app.task
def process_site(html):
    logger.info("Processing site...")
    time_matcher = re.compile(r"(?<=Last Updated: ).*")
    name_matcher = re.compile(r"(?:.*)\((?P<name>.*)\)")
    # for letter in ['ABCDEFGHIJKLMNOPQRSTUVWXYZ']:
    objects = []
    try:
        # url = "http://www.bursastation.com/prices.pl?action=prices&type=15&alphabet=ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        # # url = url_format.format(letter)
        # req = requests.get(url)
        # html = req.text

        soup = BeautifulSoup(html)
        table = soup.find_all("table", class_="TableBorderBottom")
        time_raw = soup(class_="txtBursaSummSmall")[1].text.strip()
        timestr = time_matcher.search(time_raw).group(0)
        transaction_time = datetime.strptime(timestr, "%d/%m/%Y %H:%M")
        if table:
            table = table[0]
            rows = table("tr")
            if len(rows) > 1:
                rows = rows[1:]
                for row in rows:
                    try:
                        obj = dict()
                        values = [_process_string(td.text) for td in row("td")]
                        obj['transaction_time'] = transaction_time
                        obj['name'] = name_matcher.search(values[1]).group('name')
                        obj['stock_code'] = values[2]
                        obj['volume'] = locale.atoi(values[7])
                        obj['buy_volume'] = locale.atoi(values[8])
                        obj['buy'] = int(locale.atof(values[9])*1000)
                        obj['sell'] = int(locale.atof(values[10])*1000)
                        obj['sell_volume'] = locale.atoi(values[11])
                        obj['high'] = int(locale.atof(values[12])*1000)
                        obj['low'] = int(locale.atof(values[13])*1000)
                        objects.append(obj)
                    except Exception as e:
                        logger.warning("Exception encountered while processing {0}: {1}".format(row, e))
                        continue

    except Exception as e:
        logger.exception("Exception encountered while processing site: {0}.\n{1}".format(e, html))
        return None
    else:
        logger.info("{0} objects processed.".format(len(objects)))
        return objects


@shared_task(ignore_result=True)
def dmap(iterator, callback):
    if iterator:
        # Map a callback over an iterator and return as a group
        callback = subtask(callback)
        return group(callback.clone([arg,]) for arg in iterator)()
    else:
        return None


@app.task(ignore_result=True)
def scrape(url=DEFAULT_URL):
    res = fetch_site.s(url)|process_site.s()|dmap.s(write_to_database.s())
    res.delay()


@app.task(ignore_result=True)
def test():
    logger.info("Test post please ignore.")
