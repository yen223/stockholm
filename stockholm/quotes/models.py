# import sqlalchemy as sa
# from stockholm.database import Base
from stockholm import db
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.orm import relationship, backref


class Stock(db.Model):
    __tablename__ = "stock"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    stock_code = db.Column(db.String(6))
    # description = db.Column(db.String(2048))
    def __init__(self, name=None, stock_code=None):
        self.name = name
        self.stock_code = stock_code
        
    def __repr__(self):
        return "{0}: {1}".format(self.stock_code, self.name)


class StockQuote(db.Model):
    '''
    Prices are quoted in 1/10 cents (RM0.001).
    '''
    __tablename__ = "stock_quotes"
    id = db.Column(db.Integer, primary_key=True)
    stock_id = db.Column(db.Integer, db.ForeignKey('stock.id'))
    stock = relationship("Stock", backref="stock_quotes")
    open_price = db.Column(db.Integer)
    close_price = db.Column(db.Integer)
    high = db.Column(db.Integer)
    low = db.Column(db.Integer)
    volume = db.Column(db.Integer)
    buy = db.Column(db.Integer)
    sell = db.Column(db.Integer)
    buy_volume = db.Column(db.Integer)
    sell_volume = db.Column(db.Integer)

    transaction_time = db.Column(db.DateTime)

    __table_args__ = (UniqueConstraint('stock_id', 'transaction_time'),)

    