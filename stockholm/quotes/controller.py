from flask import Flask, Blueprint, request
from flask.ext import restful
from flask.ext.restful import Resource, fields, marshal_with, reqparse, marshal
from models import StockQuote, Stock
from datetime import datetime
import time
from pandas import DataFrame


mod_quotes = Blueprint('stock_quotes', __name__, url_prefix='/quotes')
api = restful.Api()
api.init_app(mod_quotes)


api_fields = [
    StockQuote.transaction_time,
    Stock.stock_code,
    # Stock.name,
    StockQuote.high,
    StockQuote.low,
    StockQuote.buy,
    StockQuote.sell,
    StockQuote.volume,
    StockQuote.buy_volume,
    StockQuote.sell_volume,
    # StockQuote.open_price,
    # StockQuote.close_price,
]

api_columns = [
    'transaction_time',
    'stock_code',
    # 'name',
    'high',
    'low',
    'buy',
    'sell',
    'volume',
    'buy_volume',
    'sell_volume',
    # 'open_price',
    # 'close_price',
]

def parse_date(string):
    format_str = "%Y-%m-%d"
    return datetime.strptime(string, format_str)


def parse_datetime(string):
    format_str = "%Y%m%dT%H%M%S"
    return datetime.strptime(string, format_str)


def date_to_millis(d):
    """Converts a datetime object to the number of milliseconds since the unix epoch."""
    return int(time.mktime(d.timetuple())) * 1000


def get_quotes_from_daterange(stock_code, start=None, end=None):
    '''
    Get quotes in 5-minute median intervals.
    '''
    quotes = StockQuote.query.join(Stock).filter(Stock.stock_code==stock_code)
    if start:
        quotes = quotes.filter(StockQuote.transaction_time>=start)
    if end:
        quotes = quotes.filter(StockQuote.transaction_time<=end)

    dataframe = DataFrame([x.__dict__ for x in quotes])

    if len(dataframe) > 0:
    # dataframe.columns = api_columns
        dataframe.index=dataframe['transaction_time']
        dataframe.transaction_time = dataframe.transaction_time.astype(datetime)
        dataframe = dataframe.resample('5min', how='median', fill_method='pad', kind='timestamp')
        dataframe['transaction_time'] = dataframe.index.values

    return dataframe


class Timestamp(fields.Raw):
    def format(self, value):
        timestamp = int(time.mktime(value.timetuple())) * 1000
        return timestamp

quote_fields = {
    'id': fields.Integer,
    'open_price': fields.Float,
    'close_price': fields.Float,
    'high': fields.Float,
    'low': fields.Float,
    'volume': fields.Float,
    'buy': fields.Float,
    'sell': fields.Float,
    'buy_volume': fields.Float,
    'sell_volume': fields.Float,
    # 'stock_code': fields.String(attribute='stock.stock_code'),
    # 'stock_name': fields.String(attribute='stock.name'),
    'transaction_time': fields.DateTime,
    'transaction_timestamp': Timestamp(attribute='transaction_time'),
}

class Normalized(fields.Raw):
    def __init__(self, norm, *args, **kwargs):
        if norm:
            self.norm = float(norm)
        else:
            self.norm = 1
        return super(Normalized, self).__init__(*args, **kwargs)

    def format(self, value):
        return value/self.norm

def get_normalized_fields(normal_obj):
    quote_fields_normalized = {
        'id': fields.Integer,
        'open_price': Normalized(norm=normal_obj.open_price),
        'close_price': Normalized(norm=normal_obj.close_price),
        'high': Normalized(norm=normal_obj.high),
        'low': Normalized(norm=normal_obj.low),
        'volume': fields.Integer,
        'buy': Normalized(norm=normal_obj.buy),
        'sell': Normalized(norm=normal_obj.sell),
        'buy_volume': fields.Integer,
        'sell_volume': fields.Integer,
        'stock_code': fields.String(attribute='stock.stock_code'),
        'stock_name': fields.String(attribute='stock.name'),
        'transaction_time': fields.String,
        'transaction_timestamp': Timestamp(attribute='transaction_time'),
    }
    return quote_fields_normalized

quote_fields_list = {
    'code': fields.String,
    'stock_name': fields.String,
    'start': fields.DateTime,
    'end': fields.DateTime,
    'count': fields.Integer,
    'quotes': fields.Nested(quote_fields),
}

def get_normalized_list(normal_obj):
    quote_fields = get_normalized_fields(normal_obj)
    quote_fields_normalized_list = {
        'code': fields.String,
        'count': fields.Integer,
        'stock_name': fields.String,
        'start': fields.DateTime,
        'end': fields.DateTime,
        'quotes': fields.Nested(quote_fields),
    }
    return quote_fields_normalized_list

class QuoteResource(Resource):
    @marshal_with(quote_fields)
    def get(self, quote_id):
        quote = StockQuote.query.get_or_404(quote_id)
        return quote


class QuoteListResource(Resource):
    @marshal_with(quote_fields_list)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('start', type=parse_datetime)
        parser.add_argument('end', type=parse_datetime)
        parser.add_argument('code', type=str)
        parser.add_argument('normalize', type=bool)
        args = parser.parse_args()

        code = args.get('code')
        start = args.get('start')
        end = args.get('end')

        # quotes = StockQuote.query
        # if start:
        #     quotes = quotes.filter(StockQuote.transaction_time>=start)
        # if end:
        #     quotes = quotes.filter(StockQuote.transaction_time<=end)
        # if code:
        #     quotes = quotes.join(Stock).filter(Stock.stock_code==code)
        # query = quotes.limit(100).values(*api_fields)

        # dataframe = DataFrame(query)
        # dataframe.columns = api_columns
        # dataframe.index=dataframe['transaction_time']
        # dataframe.transaction_time = dataframe.transaction_time.astype(datetime)
        # dataframe = dataframe.resample('5min', how='median', fill_method='pad', kind='timestamp')
        # dataframe['transaction_time'] = dataframe.index.values
        # print dataframe.dtypes
        
        result = get_quotes_from_daterange(code, start, end)
        obj = {
            'code': code,
            'start': start,
            'end': end,
            'quotes': result.to_dict(outtype="records"),
            'stock_name': Stock.query.filter(Stock.stock_code==code).first().name,
            'count': len(result),
        }
        return obj


api.add_resource(QuoteListResource, '/api/', endpoint='quote_api_list')
api.add_resource(QuoteResource, '/api/<int:quote_id>/', endpoint='quote_api_detail')