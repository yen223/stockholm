from contextlib import contextmanager
from stockholm import db

@contextmanager
def get_session_scope():
    session = db.create_scoped_session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
    finally:
        session.remove()


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance, True