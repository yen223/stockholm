from flask.ext.sqlalchemy import SQLAlchemy
from celery import Celery
from flask import Flask


app = Flask(__name__)
db = SQLAlchemy(app)
app.config.from_object('stockholm.settings')

from quotes.controller import mod_quotes
from dashboard.controller import mod_dashboard

app.register_blueprint(mod_quotes)
app.register_blueprint(mod_dashboard)

