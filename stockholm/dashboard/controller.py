from flask import Flask, Blueprint, request, render_template
from flask.ext import restful
from flask.ext.restful import Resource, fields, marshal_with, reqparse
from stockholm.quotes.models import StockQuote, Stock
from datetime import datetime

mod_dashboard = Blueprint('dashboard', __name__, url_prefix='/')


@mod_dashboard.route('/', methods=['GET'])
def dashboard():
    return render_template("dashboard/index.html")