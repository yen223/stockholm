from __future__ import absolute_import
from celery import Celery
from celery.signals import worker_process_init, task_postrun
from stockholm import settings, db


@task_postrun.connect
def close_session(*args, **kwargs):
    db.session.remove()


app = Celery('stockholm')
app.config_from_object(settings)

if __name__ == '__main__':
    celery.start()