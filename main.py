#!/usr/bin/env python
# Run a test server.
from stockholm import app, db
# from stockholm.quotes.models import *

if __name__=="__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
